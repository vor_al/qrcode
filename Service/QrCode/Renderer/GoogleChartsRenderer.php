<?php

namespace Service\QrCode\Renderer;

/**
 * Class to render QR code using GoogleChartsRenderer
 *
 * @package Service\QrCode\Renderer
 */
class GoogleChartsRenderer extends Renderer
{

  /**
   * Base URL for getting QR data
   */
  const ROOT_URL = 'https://chart.googleapis.com/chart?cht=qr';

  /**
   * Get QR code as binary data
   *
   * @param   string  $text
   * @param   int     $width
   * @param   int     $height
   * @return  mixed
   * @throws \ErrorException if a cURL request was failed
   */
  public function generate($text, $width, $height)
  {
    $chs = '&chs=' . $width . 'x' . $height;
    $postData = array(
      'ch1' => $text
    );

    /**
     * open connection
     */
    $ch = curl_init();

    /*
     * set parameters
     */
    curl_setopt($ch, CURLOPT_URL, self::ROOT_URL . $chs);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);

    /*
     * get data from connection
     */
    $output = curl_exec($ch);

    if(curl_errno($ch)){
      throw new \ErrorException(curl_error($ch));
    }

    /**
     * close connection
     */
    curl_close($ch);

    return $output;
  }

}