<?php

namespace Service\QrCode\Renderer;

/**
 * Class Renderer - base for all renderer classes
 *
 * @package Service\QrCode\Renderer
 */
abstract class Renderer
{

  abstract public function generate($text, $width, $height);

}