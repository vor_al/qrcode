<?php

namespace Service\QrCode;

use Service\QrCode\Renderer\Renderer;

/**
 * Class to render QR code.
 *
 * @package Service\QrCode
 */
class QrCode
{

  /**
   * text for rendering
   *
   * @var string
   */
  protected $text;

  /**
   * width of QR image
   *
   * @var int
   */
  protected $width;

  /**
   * height of QR image
   *
   * @var int
   */
  protected $height;

  /**
   * object which generates QR image
   *
   * @var Renderer
   */
  protected $renderer;

  /**
   * Sets the text of QR code, sets dimensional data (width, height) of QR image
   *
   * @param string  $text
   * @param int     $width
   * @param int     $height
   */
  public function __construct($text = '', $width = 0, $height = 0)
  {
    $this->text   = $text;
    $this->width  = (int) $width;
    $this->height = (int) $height;
  }

  /**
   * Sets object, that generates QR
   *
   * @param Renderer $renderer
   */
  public function setRenderer(Renderer $renderer)
  {
    $this->renderer = $renderer;
  }

  /**
   * Return QR image
   *
   * @return mixed
   * @throws \ErrorException if renderer not defined
   */
  public function generate()
  {
    if (!$this->renderer) {
      throw new \ErrorException('Renderer does not establish. Please use setRenderer() first!');
    }
    return $this->renderer->generate($this->text, $this->width, $this->height);
  }

}
